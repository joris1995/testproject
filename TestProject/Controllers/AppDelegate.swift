//
//  AppDelegate.swift
//  TestProject
//
//  Created by Joris Dijkstra on 11/20/18
//  Copyright (c) 2018 Incentro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
